<?php

namespace App;

use BmPlatform\Abstraction\DataTypes\AppIntegrationData;
use BmPlatform\Abstraction\DataTypes\Operator as OperatorData;
use BmPlatform\Abstraction\Interfaces\AppHandler;
use BmPlatform\Abstraction\Interfaces\AppInstance;

class Hub implements \BmPlatform\Abstraction\Interfaces\Hub
{
    protected array $registeredTypes = [];

    protected array $appInstances = [];

    protected array $appHandlers = [];

    public function registerAppType(string $type, callable $createHandler)
    {
        if (isset($this->registeredTypes[$type])) throw new \RuntimeException("App type [$type] is already registered.");

        $this->registeredTypes[$type] = $createHandler;
    }

    public function createHandler(AppInstance|Models\AppInstance $appInstance): AppHandler
    {
        return match (true) {
            isset($this->appHandlers[$appInstance->id]) => $this->appHandlers[$appInstance->id],

            isset($this->registeredTypes[$appInstance->type]) =>
                $this->appHandlers[$appInstance->id] = $this->registeredTypes[$appInstance->type]($appInstance),

            default => throw new \RuntimeException("App type [{$appInstance->type}] is not registered.")
        };
    }

    public function integrate(AppIntegrationData $data): AppInstance
    {
        if (!isset($this->registeredTypes[$data->type])) {
            throw new \RuntimeException("App type [{$data->type}] is not registered.");
        }

        if (!$app = $this->findAppById($data->type, $data->externalId)) {
            $app = new Models\AppInstance();
            $app->type = $data->type;
            $app->external_id = $data->externalId;
        }

        $app->fill([
            'locale' => $data->locale,
            'tz' => $data->timeZone->getName(),
            'extra_data' => $data->extraData,
        ])->save();

        $this->appInstances[$data->type][$data->externalId] = $app;

        if ($app->wasRecentlyCreated) {
            $app->getHandler()->activate();
        }

        return $app;
    }

    public function findAppById(string $type, string $id): ?AppInstance
    {
        if (!isset($this->appInstances[$type][$id])) {
            $this->appInstances[$type][$id] = Models\AppInstance::query()->where([
                'type' => $type,
                'external_id' => $id,
            ])->first();
        }

        return $this->appInstances[$type][$id];
    }

    public function getAuthToken(AppInstance|Models\AppInstance $appInstance, OperatorData|string|null $operator = null): string
    {
        return $appInstance->id;
    }

    public function webUrl(string $authToken): string
    {
        return url('/apps/'.$authToken);
    }

    public function getCallbackId(AppInstance|Models\AppInstance $appInstance): string
    {
        return $appInstance->id;
    }

    public function findAppByCallbackId(string $type, string $id): ?AppInstance
    {
        return Models\AppInstance::query()->find($id);
    }
}
