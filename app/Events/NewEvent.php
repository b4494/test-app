<?php

namespace App\Events;

use App\Http\Resources\ChatResource;
use App\Http\Resources\ContactResource;
use App\Models\AppInstance;
use App\Models\Chat;
use App\Models\Contact;
use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(
        public readonly AppInstance $app,
        public readonly string $type,
        public readonly ?Chat $chat = null,
        public readonly ?Contact $contact = null,
        public readonly ?array $payload = null,
        public readonly ?Carbon $timestamp = null,
    ) {
        //
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('app.'.$this->app->id);
    }

    public function broadcastWith()
    {
        return [
            'appId' => $this->app->id,
            'type' => $this->type,
            'chat' => $this->chat ? ChatResource::make($this->chat)->resolve() : null,
            'contact' => $this->contact ? ContactResource::make($this->contact)->resolve() : null,
            'payload' => $this->payload,
            'timestamp' => $this->timestamp?->toIso8601String(),
        ];
    }
}
