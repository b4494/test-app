<?php

namespace App\Models;

use BmPlatform\Abstraction\Interfaces\Contact as ContactContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string|null $messenger_id
 * @property string|null $name
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $avatar
 * @property array|null $extra_fields
 */
class Contact extends Model implements ContactContract
{
    use HasFactory, AppResource, HasExtraData;

    protected $casts = [
        'extra_fields' => 'json',
        'extra_data' => 'json',
    ];

    protected $fillable = [
        'name', 'messenger_id', 'phone', 'email', 'avatar', 'extra_fields', 'extra_data',
    ];

    public function getExtraFields(): ?array
    {
        return $this->extra_fields;
    }

    public function getMessengerId(): ?string
    {
        return $this->messenger_id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getAvatarUrl(): ?string
    {
        return $this->avatar;
    }

    /**
     * @param \App\Models\AppInstance $app
     * @param \BmPlatform\Abstraction\DataTypes\Contact $data
     */
    public function fillFromData(AppInstance $app, mixed $data): void
    {
        $this->name = $data->name;
        $this->phone = $data->phone;
        $this->email = $data->email;
        $this->avatar = $data->avatarUrl;
        $this->extra_fields = $data->extraFields;
        $this->extra_data = $data->extraData;
        $this->messenger_id = $data->messengerId;
    }
}
