<?php

namespace App\Models;

use BmPlatform\Abstraction\Interfaces\AppHandler;
use BmPlatform\Abstraction\Interfaces\AppInstance as AppInstanceContract;
use BmPlatform\Support\Facades\Hub;
use Carbon\CarbonTimeZone;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property string $type
 * @property string $external_id
 * @property string $locale
 * @property string $tz
 * @property bool $is_active
 * @property \Illuminate\Database\Eloquent\Collection|\App\Models\Messenger[] $messengers
 * @property \Illuminate\Database\Eloquent\Collection|\App\Models\Chat[] $chats
 * @property \Illuminate\Database\Eloquent\Collection|\App\Models\Contact[] $contacts
 */
class AppInstance extends Model implements AppInstanceContract
{
    use HasFactory, HasExtraData;

    protected $casts = [
        'extra_data' => 'json',
        'is_active' => 'bool',
    ];

    protected $fillable = [
        'locale', 'tz', 'is_active', 'extra_data',
    ];

    public function messengers(): HasMany
    {
        return $this->hasMany(Messenger::class, 'app_id', 'id');
    }

    public function contacts(): HasMany
    {
        return $this->hasMany(Contact::class, 'app_id', 'id');
    }

    public function chats(): HasMany
    {
        return $this->hasMany(Chat::class, 'app_id', 'id');
    }

    public function logs(): HasMany
    {
        return $this->hasMany(Log::class, 'app_id', 'id');
    }

    public function getAppType(): string
    {
        return $this->type;
    }

    public function getExternalId(): string
    {
        return $this->external_id;
    }

    public function getHandler(): AppHandler
    {
        return Hub::createHandler($this);
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function getTimezone(): CarbonTimeZone
    {
        return CarbonTimeZone::instance($this->tz);
    }

    public function isActive(): bool
    {
        return $this->is_active;
    }
}
