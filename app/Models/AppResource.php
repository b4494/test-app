<?php

namespace App\Models;

use BmPlatform\Abstraction\DataTypes\MessengerInstance as MessengerInstanceData;
use Illuminate\Database\Eloquent\Model;

/**
 * @property \App\Models\AppInstance $app
 * @property int $app_id
 * @property string $external_id
 */
trait AppResource
{
    public function app()
    {
        return $this->belongsTo(AppInstance::class, 'app_id', 'id', 'app');
    }

    public function getExternalId(): string
    {
        return $this->external_id;
    }

    public static function import(AppInstance $app, mixed $data): ?static
    {
        if (!$data) return null;

        /** @var self $model */
        if (!$model = static::query()->where([
            'app_id' => $app->id, 'external_id' => is_string($data) ? $data : $data->externalId
        ])->first()) {
            if (is_string($data)) {
                throw new \RuntimeException(class_basename(static::class).' cannot be imported by external id since it does\'nt exists yet');
            }

            $model = new static;

            $model->external_id = $data->externalId;

            $model->app()->associate($app);
        }

        if (is_string($data)) return $model;

        $model->fillFromData($app, $data);

        $model->save();

        return $model;
    }

    abstract public function fillFromData(AppInstance $app, mixed $data): void;
}
