<?php

namespace App\Models;

use BmPlatform\Abstraction\DataTypes\MessengerInstance as MessengerInstanceData;
use BmPlatform\Abstraction\Enums\MessengerType;
use BmPlatform\Abstraction\Interfaces\MessengerInstance;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property MessengerType|null $internal_type
 * @property string $external_type
 * @property string|null $name
 * @property string|null $description
 * @property string|null $messenger_id
 */
class Messenger extends Model implements MessengerInstance
{
    use HasFactory, HasExtraData, AppResource;

    protected $casts = [
        'internal_type' => MessengerType::class,
        'extra_data' => 'json',
    ];

    public function getInternalType(): ?MessengerType
    {
        return $this->internal_type;
    }

    public function getMessengerId(): ?string
    {
        return $this->messenger_id;
    }

    public function getExternalType(): string
    {
        return $this->external_type;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param \App\Models\AppInstance $app
     * @param \BmPlatform\Abstraction\DataTypes\MessengerInstance $data
     */
    public function fillFromData(AppInstance $app, mixed $data): void
    {
        $this->name = $data->name;
        $this->description = $data->description;
        $this->internal_type = $data->internalType;
        $this->external_type = $data->externalType;
        $this->messenger_id = $data->messengerId;
        $this->extra_data = $data->extraData;
    }
}
