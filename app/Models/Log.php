<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $app_id
 * @property \App\Models\AppInstance $appInstance
 * @property int|null $chat_id
 * @property \App\Models\Chat|null $chat
 * @property int|null $contact_id
 * @property \App\Models\Contact|null $contact
 * @property string $type
 * @property mixed $payload
 * @property \Carbon\Carbon|null $timestamp
 */
class Log extends Model
{
    use HasFactory;

    protected $casts = [
        'payload' => 'json',
        'timestamp' => 'datetime',
    ];

    public function app()
    {
        return $this->belongsTo(AppInstance::class, 'app_id', 'id', 'app');
    }

    public function contact(): BelongsTo
    {
        return $this->belongsTo(Contact::class, 'contact_id', 'id', 'contact');
    }

    public function chat(): BelongsTo
    {
        return $this->belongsTo(Chat::class, 'chat_id', 'id', 'contact');
    }
}
