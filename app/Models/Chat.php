<?php

namespace App\Models;

use BmPlatform\Abstraction\Interfaces\Chat as ChatContract;
use BmPlatform\Abstraction\Interfaces\Contact;
use BmPlatform\Abstraction\Interfaces\MessengerInstance;
use BmPlatform\Abstraction\Interfaces\Operator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property \App\Models\Messenger $messengerInstance
 * @property int $messenger_instance_id
 * @property string|null $messenger_id
 * @property \App\Models\Contact|null $contact
 * @property int|null $contact_id
 */
class Chat extends Model implements ChatContract
{
    use HasFactory, AppResource, HasExtraData;

    protected $casts = [
        'extra_data' => 'json',
    ];

    public function messengerInstance(): BelongsTo
    {
        return $this->belongsTo(Messenger::class, 'messenger_instance_id', 'id', 'messengerInstance');
    }

    public function getMessengerInstance(): MessengerInstance
    {
        return $this->messengerInstance;
    }

    public function getMessengerId(): ?string
    {
        return $this->messenger_id;
    }

    public function contact(): BelongsTo
    {
        return $this->belongsTo(\App\Models\Contact::class, 'contact_id', 'id', 'contact');
    }

    public function getContact(): ?Contact
    {
        return $this->contact;
    }

    public function getOperator(): ?Operator
    {
        // TODO: Implement getOperator() method.
        return null;
    }

    /**
     * @param \App\Models\AppInstance $app
     * @param \BmPlatform\Abstraction\DataTypes\Chat $data
     */
    public function fillFromData(AppInstance $app, mixed $data): void
    {
        $this->messengerInstance()->associate(Messenger::import($app, $data->messengerInstance));

        if ($contact = \App\Models\Contact::import($app, $data->contact)) {
            $this->contact()->associate($contact);
        } else {
            $this->contact()->dissociate();
        }

        $this->messenger_id = $data->messengerId;
        $this->extra_data = $data->extraData;
    }
}
