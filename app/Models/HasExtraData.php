<?php

namespace App\Models;

/**
 * @property mixed $extra_data
 */
trait HasExtraData
{
    public function setExtraData(mixed $value)
    {
        $this->extra_data = $value;
    }

    public function getExtraData(): mixed
    {
        return $this->extra_data;
    }
}
