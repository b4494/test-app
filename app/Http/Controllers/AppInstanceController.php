<?php

namespace App\Http\Controllers;

use App\Models\AppInstance;

class AppInstanceController extends Controller
{
    public function __invoke(AppInstance $appInstance)
    {
        return view('app', compact('appInstance'));
    }
}
