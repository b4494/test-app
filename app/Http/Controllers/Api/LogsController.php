<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\LogResource;
use App\Models\AppInstance;
use App\Models\Log;
use Illuminate\Http\Request;

class LogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AppInstance $app, Request $request)
    {
        return LogResource::collection($app->logs()->with('contact', 'chat')->latest()->paginate(
            perPage: $request->get('itemsPerPage', 15),
        ));
    }
}
