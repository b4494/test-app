<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AppInstanceResource;
use App\Models\AppInstance;

class AppInstanceController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AppInstance  $app
     *
     * @return \Illuminate\Http\Response
     */
    public function show(AppInstance $app)
    {
        $app->load([ 'messengers' ]);

        return AppInstanceResource::make($app);
    }

}
