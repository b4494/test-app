<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MessengerResource;
use App\Models\AppInstance;
use App\Models\Messenger;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class MessengersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AppInstance $app)
    {
        return MessengerResource::collection($app->messengers);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Messenger  $messenger
     * @return \Illuminate\Http\Response
     */
    public function show(Messenger $messenger)
    {
        return MessengerResource::make($messenger);
    }

    public function import(AppInstance $app)
    {
        $cursor = $app->getHandler()->getMessengerInstances();

        Model::unguard();

        /** @var \BmPlatform\Abstraction\DataTypes\MessengerInstance $item */
        /** @var \App\Models\Messenger $model */
        foreach ($cursor->data as $item) {
            Messenger::import($app, $item);
        }

        return $this->index($app);
    }
}
