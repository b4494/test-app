<?php

namespace App\Http\Controllers\Api;

use App\Events\NewEvent;
use App\Http\Controllers\Controller;
use App\Models\Chat;
use BmPlatform\Abstraction\Requests\SendTextMessageRequest;
use Illuminate\Http\Request;

class RunActionController extends Controller
{
    public function __invoke(Chat $chat, string $action, Request $request)
    {
        if (!method_exists($this, $action)) {
            throw new \Exception("Action [$action] is not supported.");
        }

        $this->$action($chat, $request);
    }

    protected function sendText(Chat $chat, Request $request)
    {
        $this->validate($request, [ 'text' => 'required' ]);

        $req = new SendTextMessageRequest($chat, $request->text);

        $resp = $chat->app->getHandler()->getCommands()->sendTextMessage($req);

        $chat->touch();

        event(new NewEvent($chat->app, 'textSent', $chat, payload: [
            'request' => [
                'text' => $req->text,
                'quickReplies' => $req->quickReplies,
                'inlineButtons' => $req->inlineButtons,
            ],

            'response' => [
                'externalId' => $resp->externalId,
                'messengerId' => $resp->messengerId,
            ],
        ]));
    }
}
