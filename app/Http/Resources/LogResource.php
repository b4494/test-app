<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\Log
 */
class LogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            $this->merge($this->attributesToArray()),
            'chat' => $this->whenLoaded('chat', ChatResource::make($this->chat)),
            'contact' => $this->whenLoaded('contact', ContactResource::make($this->contact)),
        ];
    }
}
