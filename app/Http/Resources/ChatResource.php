<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\Chat
 */
class ChatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            $this->merge($this->attributesToArray()),
            'contact' => ContactResource::make($this->whenLoaded('contact')),
            'messengerInstance' => MessengerResource::make($this->whenLoaded('messengerInstance')),
        ];
    }
}
