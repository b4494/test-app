<?php

namespace App\Listeners;

use App\Events\NewEvent;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\Enums\InboxFlags;
use BmPlatform\Abstraction\Events\InboxReceived;
use App\Models\Chat;
use App\Models\Contact;

class HandleInboxReceived
{
    /**
     * Handle the event.
     *
     * @param  \BmPlatform\Abstraction\Events\InboxReceived  $event
     * @return void
     */
    public function handle(InboxReceived $event)
    {
        $part = Contact::import($event->moduleInstance, $event->participant);
        $chat = Chat::import($event->moduleInstance, $event->chat);

        event(new NewEvent($event->moduleInstance, 'inboxReceived', $chat, $part, payload: [
            'message' => $event->message->toArray(),
            'externalItem' => $event->externalItem?->toArray(),
            'flags' => $this->mapFlags($event->flags),
        ], timestamp: $event->timestamp));
    }

    protected function mapFlags(?int $flags): array
    {
        $flags = $flags ?: 0;

        return [
            'newTicket' => ($flags&InboxFlags::NEW_TICKET_OPENED) > 0,
            'newChat' => ($flags&InboxFlags::NEW_CHAT_CREATED) > 0,
            'isComment' => ($flags&InboxFlags::EXTERNAL_POST_COMMENT) > 0,
        ];
    }
}
