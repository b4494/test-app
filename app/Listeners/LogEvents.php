<?php

namespace App\Listeners;

use App\Events\NewEvent;
use App\Models\Log;

class LogEvents
{
    public function handle(NewEvent $event)
    {
        $log = new Log();
        $log->app_id = $event->app->id;
        $log->type = $event->type;
        $log->timestamp = $event->timestamp;
        $log->contact_id = $event->contact?->id;
        $log->chat_id = $event->chat?->id;
        $log->payload = $event->payload;
        $log->save();

        logger($event->type, [
            'appId' => $event->app->id,
            'chatId' => $event->chat?->id,
            'contactId' => $event->contact?->id,
            'payload' => $event->payload,
            'ts' => $event->timestamp?->toIso8601String(),
        ]);
    }
}
