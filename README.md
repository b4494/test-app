## Requirements

- PHP 8.1
- Node 14

## Installation

```shell
composer install
```

If you are planning to modify assets:

```shell
npm i && npx run mix
```

## Usage

This application uses WebSockets. Websocket is listening on `127.0.0.1:6001`. To run websocket server:

```shell
php artisan websockets:serve
```
