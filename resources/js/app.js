import './bootstrap';
import vuetify from './vuetify';
import Vue from 'vue';
import AppView from './AppView';

const app = new Vue({
    components: { AppView },
    vuetify,
    el: '#app',
    data: () => ({
        error: null,
    }),
});

axios.interceptors.response.use(null, function (error) {
    app.error = error?.response.data.message || 'Unknown error';

    return error;
});
