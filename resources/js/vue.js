import vue from 'vue';
import moment from 'moment';

vue.filter('relative_time', function (val) {
    return moment(val).calendar();
});
