<?php /** @var \App\Models\AppInstance $appInstance */ ?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">

    <link rel="stylesheet" href="/css/app.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@6.x/css/materialdesignicons.min.css" rel="stylesheet">
</head>
<body>
<div id="app">
    <v-app>
        <v-main>
            <v-container>
                <v-alert v-if="error" type="error" outlined>@{{ error }}</v-alert>
                <app-view :id="{{ $appInstance->id }}" />
            </v-container>
        </v-main>
    </v-app>
</div>
<script src="/js/app.js"></script>
</body>
</html>
