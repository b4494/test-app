<?php

use App\Http\Controllers\Api\AppInstanceController;
use App\Http\Controllers\Api\ChatsController;
use App\Http\Controllers\Api\ContactsController;
use App\Http\Controllers\Api\MessengersController;
use App\Http\Controllers\Api\RunActionController;
use App\Http\Controllers\Api\LogsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('apps', AppInstanceController::class)->only('show');

Route::post('apps/{app}/messengers/import', [ MessengersController::class, 'import' ]);
Route::apiResource('apps.messengers', MessengersController::class)->only([ 'show', 'index' ])->shallow();

Route::apiResource('apps.logs', LogsController::class)->only([ 'index' ]);

Route::apiResource('apps.contacts', ContactsController::class)->only([ 'show', 'index' ])->shallow();

Route::post('chats/{chat}/run/{action}', RunActionController::class);
Route::apiResource('apps.chats', ChatsController::class)->only([ 'show', 'index' ])->shallow();
