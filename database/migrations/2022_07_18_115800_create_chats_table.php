<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('app_id');
            $table->string('external_id');
            $table->unsignedBigInteger('messenger_instance_id');
            $table->unsignedBigInteger('contact_id')->nullable();
            $table->unsignedBigInteger('operator_id')->nullable();
            $table->string('messenger_id');
            $table->json('extra_data')->nullable();
            $table->timestamps();

            $table->unique([ 'app_id', 'external_id' ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
    }
};
