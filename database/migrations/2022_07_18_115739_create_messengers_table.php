<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messengers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('app_id');
            $table->string('external_id');
            $table->string('external_type');
            $table->string('messenger_id')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->string('internal_type')->nullable();
            $table->json('extra_data')->nullable();
            $table->timestamps();

            $table->index([ 'app_id', 'external_id' ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messengers');
    }
};
