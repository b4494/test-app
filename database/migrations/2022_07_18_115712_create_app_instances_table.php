<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_instances', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('external_id');
            $table->char('locale', 2);
            $table->string('tz', 20);
            $table->boolean('is_active')->default(true);
            $table->json('extra_data')->nullable();
            $table->timestamps();

            $table->unique([ 'type', 'external_id' ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_instances');
    }
};
